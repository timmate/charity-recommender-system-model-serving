import os

import implicit
import mlflow
import numpy as np
import pandas as pd
from dotenv import find_dotenv, load_dotenv
from fastapi import FastAPI

import utils


load_dotenv(find_dotenv())

DATA_FILE = utils.get_project_root() / 'data' / 'user_item_matrix.csv'  # TODO: move to data

# THRESHOLD = 10  # min required num of relevant items per user
# N_RELEVANT_ITEMS_TO_HAVE_IN_TEST_SET_PER_USER = 3
# K = 10  # items to recommend per user
THRESHOLD = 5  # min required num of relevant items per user
N_RELEVANT_ITEMS_TO_HAVE_IN_TEST_SET_PER_USER = 2
K = 10  # items to recommend per user

RANDOM_STATE = 42
ALS_NUM_FACTORS = 5


class Model:
    def __init__(self):
        self._model = self.load_model()

    def load_model(self):
        mlflow.set_tracking_uri(os.getenv('MLFLOW_ENDPOINT_URL'))
        model = mlflow.sklearn.load_model(os.getenv('MLFLOW_MODEL_LOADING_PATH'))
        print('loaded model from mlflow')
        return model

    def predict(self, user_id, user_items, num_preds=5):
        preds = self._model.recommend(userid=user_id, user_items=user_items, N=num_preds)[0].tolist()
        print(type(preds))
        print(preds)
        return {'preds': preds}


def get_user_item_matrix(data_file, threshold):
    user_item_matrix: pd.DataFrame = pd.read_csv(data_file)
    print('user-item matrix shape before filtering by threshold:', user_item_matrix.shape)
    user_item_matrix = user_item_matrix[user_item_matrix.sum(axis=1) >= threshold]
    user_item_matrix = user_item_matrix.reset_index(drop=True)
    print('user-item matrix shape after filtering by threshold:', user_item_matrix.shape)
    return user_item_matrix


def init():
    global model, train_matrix, test_matrix
    model = Model()
    user_item_matrix = get_user_item_matrix(DATA_FILE, THRESHOLD)
    train_matrix, test_matrix = utils.leave_k_out(
        user_item_matrix, k=N_RELEVANT_ITEMS_TO_HAVE_IN_TEST_SET_PER_USER,
        threshold=THRESHOLD, seed=RANDOM_STATE
    )
    # make sure the total num of relevant items for each user is still the same
    assert np.all(user_item_matrix.sum(axis=1).to_numpy() == 
                  (train_matrix.sum(axis=1) + test_matrix.sum(axis=1)).flatten())


app = FastAPI()

model = None
train_matrix = None
test_matrix = None
init()


@app.post("/predict")
def predict(user_id: int, num_preds: int = 5):
    if model is None or train_matrix is None:
        raise Exception("Model is not initialized, please run the init function")
    return model.predict(user_id, train_matrix[user_id], num_preds)


if __name__ == '__main__':
    print('Please run this program as a FastAPI program using uvicorn, Exiting...')
    import sys
    sys.exit(1)