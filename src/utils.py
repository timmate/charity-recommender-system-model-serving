import os
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix


def get_project_root():
    current_file_path = Path(__file__).resolve()
    # Assuming your utility file is in src/utils, go up two levels to reach the root folder
    root_folder = current_file_path.parents[1]
    return root_folder


def leave_k_out(user_item_matrix: pd.DataFrame, k=2, threshold=5, seed=None):
    """
    Split the user-item matrix into training and test sets using the leave-k-out strategy,
    i.e., keeping k relevant items for each user in the test set whenever enough relevant items
    (equal to or more than the threshold) are available for that user.
    """

    np.random.seed(seed)

    sparse_user_item_matrix = csr_matrix(user_item_matrix)
    train_matrix = sparse_user_item_matrix.copy()
    test_matrix = csr_matrix(sparse_user_item_matrix.shape, dtype=np.float64)

    for user_id in range(sparse_user_item_matrix.shape[0]):
        user_interactions = sparse_user_item_matrix[user_id].indices
        if len(user_interactions) >= threshold:
            test_items = np.random.choice(user_interactions, size=min(k, len(user_interactions)), replace=False)
            train_matrix[user_id, test_items] = 0
            test_matrix[user_id, test_items] = sparse_user_item_matrix[user_id, test_items]

    train_matrix.eliminate_zeros()
    return train_matrix, test_matrix
